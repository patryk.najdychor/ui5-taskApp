sap.ui.define([
    "sap/ui/core/mvc/Controller",
	"sap/ui/util/Storage",
	"sap/ui/core/UIComponent"
], function(Controller, Storage, UIComponent) {
    "use strict";

    var task;
    var storage;

    function onRouteMatched(event) {
        storage = new Storage(Storage.Type.local, "ui5TaskList");
        var taskId = event.getParameter("arguments").taskId;
        taskId = Number(taskId);
        var tasks = sap.ui.getCore().getModel("tasks").getData().tasks;
        for (var i = 0; i < tasks.length; i++) {
            var t = tasks[i];
            if (t.id != taskId) { continue; }
            task = t;
            break
        }

        if (!task) {
            UIComponent.getRouterFor(this).navTo("notFound");
            return;
        }

        updateView();
    }

    function updateStorage() {
        var tasks = sap.ui.getCore().getModel("tasks").getJSON();
        storage.put("tasks", tasks);
    }

    function updateTasksModel() {
        var tasksModel = sap.ui.getCore().getModel("tasks");
        var tasksModelData = tasksModel.getData();
        var tasks = tasksModelData.tasks;
        for (var i = 0; i < tasks.length; i++) {
            var t = tasks[i];
            if (t.id != task.id) { continue; }
            tasks[i] = task;
            break
        }
        tasksModelData.tasks = tasks;
        tasksModel.setData(tasksModelData);
    }

    function updateView() {
        var view = this.getView();
        view.byId("nameLabel").setValue(task.name);
        view.byId("descriptionLabel").setValue(task.description);

        view.byId("datePicker").setValue(task.date);
    }

    return Controller.extend("digital.pilcrow.taskApp.controller.Details", {
        onInit: function() {
            onRouteMatched = onRouteMatched.bind(this);
            updateTasksModel = updateTasksModel.bind(this);
            updateView = updateView.bind(this);

            this.getOwnerComponent().getRouter().getRoute("details").attachMatched(onRouteMatched, this);
        },

        onDatePickerChange: function(event) {
            var dateString = this.getView().byId("datePicker").getDateValue().toDateString();
            task.date = dateString;
            updateTasksModel();
            updateStorage();
        },

        onDescriptionLabelLiveChange: function(event) {
            var labelValue = event.getParameter("value");
            task.description = labelValue;
            updateTasksModel();
            updateStorage();
        },

        onNameLabelLiveChange: function(event) {
            var labelValue = event.getParameter("value");
            task.name = labelValue;
            updateTasksModel();
            updateStorage();
        },

        onNavButtonPress: function(event) {
            UIComponent.getRouterFor(this).navTo("home");
        }
    });
});