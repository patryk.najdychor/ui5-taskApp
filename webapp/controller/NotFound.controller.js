sap.ui.define([
    "sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent"
], function(Controller, UIComponent) {
    "use strict";

    return Controller.extend("digital.pilcrow.taskApp.controller.NotFound", {
        onNavButtonPress: function() {
            UIComponent.getRouterFor(this).navTo("home");
        }
    })
})