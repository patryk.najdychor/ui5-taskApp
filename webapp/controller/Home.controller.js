sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel",
	"sap/ui/util/Storage"
], function(Controller, MessageToast, JSONModel, Storage) {
	"use strict";

	return Controller.extend("digital.pilcrow.taskApp.controller.Home", {
		_getTaskIndex: function (taskId) {
			var taskData = this.getView().getModel("tasks").getData();
			var taskIndex = -1

			for (var i = 0; i < taskData.tasks.length; i++) {
				var task = taskData.tasks[i];
				if (task.id === taskId) {
					taskIndex = i;
					break;
				}
			}

			return taskIndex;
		},

		_getCalendarSelection: function () {
			var selectedDate = this.byId("calendar").getSelectedDates()[0]
			if (!selectedDate) {
				return new Date().toDateString();
			}
			return selectedDate.getStartDate().toDateString();
		},

		_updateDateSelectionLabel: function () {
			this.getView().byId("taskDateLabel").setText(this._getText("selectedDate") + " " + this._getCalendarSelection());
		},

		_getText: function (textName) {
			return this.getOwnerComponent().getModel("i18n").getProperty(textName);
		},

		onCalendarSelect: function (event) {
			this._updateDateSelectionLabel();

			//update filter when date selection changes
			var taskList = this.getView().byId("taskList");
			var filter = new sap.ui.model.Filter("date", sap.ui.model.FilterOperator.Contains, this._getCalendarSelection());
			taskList.getBinding("items").filter([filter], sap.ui.model.FilterType.Application);
		},

		onInit: function () {
			this.storage = new Storage(Storage.Type.local, "ui5TaskList");
			var taskModel = sap.ui.getCore().getModel("tasks");
			this.getView().setModel(taskModel, "tasks");
			this.onCalendarSelect();
		},

		onButtonPress: function () {
			var taskName = this.byId("taskNameInput").getValue();

			if (taskName === "") {
				var taskAddedFailure = this._getText("taskFailureEmptyName");
				MessageToast.show(taskAddedFailure);
				return;
			}

			var taskDescription = this.byId("taskDescriptionInput").getValue();

			var taskModelData = this.getView().getModel("tasks").getData();
			var newTask = {
				id: Math.random(),
				name: taskName,
				description: taskDescription,
				date: this._getCalendarSelection()
			};
			taskModelData.tasks.push(newTask);
			this.getView().getModel("tasks").setData(taskModelData);
			this._updateStorage();

			this.byId("taskNameInput").setValue("");
			this.byId("taskDescriptionInput").setValue("");

			MessageToast.show(this._getText("taskSuccess"));
		},

		onTaskDelete: function (event) {
			var taskId = event.getParameter("listItem").data("taskId");
			var taskIndex = this._getTaskIndex(taskId);
			//task not found
			if (taskIndex === -1) {
				return;
			}

			var tasksData = this.getView().getModel("tasks").getData();
			tasksData.tasks.splice(taskIndex, 1);
			this.getView().getModel("tasks").setData(tasksData);

			this._updateStorage();
			MessageToast.show(this._getText("taskDeleted"));
		},

		onTaskPress: function (event) {
			var taskId = event.getSource().data("taskId");

			this.getOwnerComponent().getRouter().navTo("details", {
				taskId: taskId
			});
		},

		_updateStorage: function () {
			var tasks = this.getView().getModel("tasks").getJSON();
			this.storage.put("tasks", tasks);
		}
	});
});