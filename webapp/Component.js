sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"./model/models",
	"sap/ui/util/Storage",
	"sap/ui/model/json/JSONModel"
], function(UIComponent, Device, models, Storage, JSONModel) {
	"use strict";

	return UIComponent.extend("digital.pilcrow.taskApp.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			var storage = new Storage(Storage.Type.local, "ui5TaskList");
			var tasks = storage.get("tasks");

			var taskModel = new JSONModel();
			taskModel.setJSON(tasks);

			//first run - storage was empty
			if (!taskModel.getData()) {
				taskModel.setData({
					tasks: []
				});
			}

			//deserialize id
			var taskModelData = taskModel.getData();
			for (var i = 0; i < taskModelData.tasks.length; i++) {
				taskModelData.tasks[i].id = Number(taskModelData.tasks[i].id)
			}

			sap.ui.getCore().setModel(taskModel, "tasks");

			// create the views based on the url/hash
			this.getRouter().initialize();
		}
	});
});