# UI5 Task App
Allows to create calendar-based task list.
Saves all data to local storage.

## What I learned
 - MVC
 - Routing
 - JSONModel
 - i18n
 - local storage

Developed using OpenUI5 framework.

Check it out here:
https://pilcrow.digital/demo/taskApp/index.html
